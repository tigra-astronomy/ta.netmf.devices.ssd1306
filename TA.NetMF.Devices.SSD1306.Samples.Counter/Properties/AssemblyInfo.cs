﻿// This file is part of the TA.NetMF.SSD1306 project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: AssemblyInfo.cs  Last modified: 2015-09-11@05:55 by Tim Long

using System.Reflection;

[assembly: AssemblyTitle("Counter sample")]
[assembly: AssemblyDescription("Shows a 5-digit incrementing counter in large font.")]