﻿// This file is part of the TA.NetMF.SSD1306 project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: Program.cs  Last modified: 2015-09-11@05:55 by Tim Long

using Microsoft.SPOT;
using Microsoft.SPOT.Hardware;
using TA.NetMF.Devices.Ssd1306;
using TA.NetMF.Devices.SSD1306.Samples.Counter;
using TA.NetMF.Utilities.I2C;

namespace TA.NetMF.SSD1306
    {
    public class Program
        {
        const int IicTimeout = 400;

        public static void Main()
            {
            var iicDisplayConfig = new I2CDevice.Configuration(0x3C, 400);
            var iic = new ThreadSafeI2CDevice(iicDisplayConfig, timeout: IicTimeout, retryLimit: 3);
            var display = new DisplayDriver(iic);
            display.PowerUp();
            display.ClearScreen();
            Debug.EnableGCMessages(true);
            Debug.GC(true);
            var bigFont = new OcrA24PtExtended();
            var smallFont = new LucidaConsole12Pt();
            display.RenderString(smallFont, 6, 4, "Microns Steps");
            var i = 0;
            while (true)
                {
                display.RenderString(bigFont, 1, 8, i.ToString("D5"));
                if (++i > 99999) i = 0;
                //Thread.Sleep(1000);
                //display.TestMode();
                //Thread.Sleep(1000);
                //display.CancelTestMode();
                }
            }
        }
    }