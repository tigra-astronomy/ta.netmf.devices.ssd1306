using System;
using System.Collections;
using Microsoft.SPOT;
using TA.NetMF.Devices.Ssd1306;

namespace TA.NetMF.Devices.SSD1306.Samples.Counter
    {
    public class LucidaConsole12Pt : BitMappedFont
        {
        public LucidaConsole12Pt()
            {
            CellPageHeight = 2;
            Characters = new Hashtable {
        {' ', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                }
            }},
        {'!', new BitMappedCharacter
            {
            Width = 1,
            Stripes = new byte[]
                {
	//  
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	//  
	// #
	// #
	//  
	//  
	//  
	0xFE,
    0x1B,
                }
            }},
        {'"', new BitMappedCharacter
            {
            Width = 5,
            Stripes = new byte[]
                {
	//      
	// #   #
	// #   #
	// #   #
	// #   #
	//      
	//      
	//      
	//      
	//      
	//      
	//      
	//      
	//      
	//      
	//      
	0x1E, 0x00, 0x00, 0x00, 0x1E,
    0x00, 0x00, 0x00, 0x00, 0x00,
                }
            }},
        {'#', new BitMappedCharacter
            {
            Width = 9,
            Stripes = new byte[]
                {
	//          
	//     #  # 
	//     #  # 
	//    #  #  
	//    #  #  
	//  ########
	//    #  #  
	//   #  #   
	// #########
	//   #  #   
	//   #  #   
	//  #  #    
	//  #  #    
	//          
	//          
	//          
	0x00, 0x20, 0xA0, 0x78, 0x26, 0xA0, 0x78, 0x26, 0x20,
    0x01, 0x19, 0x07, 0x01, 0x19, 0x07, 0x01, 0x01, 0x01,
                }
            }},
        {'$', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//    #  
	//  #####
	// ## #  
	// #  #  
	// #  #  
	//  # #  
	//   ##  
	//    ## 
	//    ###
	//    # #
	//    # #
	// #  # #
	// ##### 
	//    #  
	//       
	//       
	0x1C, 0x26, 0x42, 0xFF, 0x82, 0x02,
    0x18, 0x10, 0x10, 0x3F, 0x11, 0x0F,
                }
            }},
        {'%', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//  ###     #
	// #   #   # 
	// #   #  #  
	// #   # #   
	//  ###  #   
	//      #    
	//     #     
	//    #  ### 
	//    # #   #
	//   #  #   #
	//  #   #   #
	// #     ### 
	//           
	//           
	//           
	0x1C, 0x22, 0x22, 0x22, 0x9C, 0x40, 0x30, 0x08, 0x04, 0x02,
    0x10, 0x08, 0x04, 0x03, 0x00, 0x0E, 0x11, 0x11, 0x11, 0x0E,
                }
            }},
        {'&', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//    ###    
	//   #   #   
	//   #   #   
	//   #  ##   
	//    ###    
	//   ##      
	//  #  #    #
	// #   ##   #
	// #    #  # 
	// #     # # 
	//  #    ##  
	//   ####### 
	//           
	//           
	//           
	0x00, 0x80, 0x5C, 0x62, 0xA2, 0x32, 0x1C, 0x00, 0x00, 0x80,
    0x07, 0x08, 0x10, 0x10, 0x11, 0x13, 0x1C, 0x18, 0x16, 0x01,
                }
            }},
        {'\'', new BitMappedCharacter
            {
            Width = 1,
            Stripes = new byte[]
                {
	//  
	// #
	// #
	// #
	// #
	//  
	//  
	//  
	//  
	//  
	//  
	//  
	//  
	//  
	//  
	//  
	0x1E,
    0x00,
                }
            }},
        {'(', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//       
	//     ##
	//   ##  
	//  ##   
	//  #    
	// #     
	// #     
	// #     
	// #     
	// #     
	// #     
	//  #    
	//  ##   
	//   ##  
	//     ##
	//       
	0xE0, 0x18, 0x0C, 0x04, 0x02, 0x02,
    0x07, 0x18, 0x30, 0x20, 0x40, 0x40,
                }
            }},
        {')', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//       
	// ##    
	//   ##  
	//    ## 
	//     # 
	//      #
	//      #
	//      #
	//      #
	//      #
	//      #
	//     # 
	//    ## 
	//   ##  
	// ##    
	//       
	0x02, 0x02, 0x04, 0x0C, 0x18, 0xE0,
    0x40, 0x40, 0x20, 0x30, 0x18, 0x07,
                }
            }},
        {'*', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//    #   
	//    #   
	// ## # ##
	//  ## ## 
	//   # #  
	//   ###  
	//   # #  
	//        
	//        
	//        
	//        
	//        
	//        
	//        
	//        
	0x08, 0x18, 0xF0, 0x4E, 0xF0, 0x18, 0x08,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
                }
            }},
        {'+', new BitMappedCharacter
            {
            Width = 9,
            Stripes = new byte[]
                {
	//          
	//          
	//          
	//          
	//          
	//     #    
	//     #    
	//     #    
	//     #    
	// #########
	//     #    
	//     #    
	//     #    
	//          
	//          
	//          
	0x00, 0x00, 0x00, 0x00, 0xE0, 0x00, 0x00, 0x00, 0x00,
    0x02, 0x02, 0x02, 0x02, 0x1F, 0x02, 0x02, 0x02, 0x02,
                }
            }},
        {',', new BitMappedCharacter
            {
            Width = 2,
            Stripes = new byte[]
                {
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	// ##
	// ##
	//  #
	//  #
	// # 
	0x00, 0x00,
    0x98, 0x78,
                }
            }},
        {'-', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	// ######
	//       
	//       
	//       
	//       
	//       
	//       
	//       
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
                }
            }},
        {'.', new BitMappedCharacter
            {
            Width = 2,
            Stripes = new byte[]
                {
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	// ##
	// ##
	//   
	//   
	//   
	0x00, 0x00,
    0x18, 0x18,
                }
            }},
        {'/', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//        #
	//       # 
	//       # 
	//      #  
	//      #  
	//     #   
	//     #   
	//    #    
	//    #    
	//   #     
	//   #     
	//  #      
	//  #      
	// #       
	//         
	0x00, 0x00, 0x00, 0x00, 0xC0, 0x30, 0x0C, 0x02,
    0x40, 0x30, 0x0C, 0x03, 0x00, 0x00, 0x00, 0x00,
                }
            }},
        {'0', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//   ###  
	//  #   # 
	//  #   # 
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	//  #   # 
	//  #   # 
	//   ###  
	//        
	//        
	//        
	0xF0, 0x0C, 0x02, 0x02, 0x02, 0x0C, 0xF0,
    0x03, 0x0C, 0x10, 0x10, 0x10, 0x0C, 0x03,
                }
            }},
        {'1', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//    #   
	// ####   
	//    #   
	//    #   
	//    #   
	//    #   
	//    #   
	//    #   
	//    #   
	//    #   
	//    #   
	// #######
	//        
	//        
	//        
	0x04, 0x04, 0x04, 0xFE, 0x00, 0x00, 0x00,
    0x10, 0x10, 0x10, 0x1F, 0x10, 0x10, 0x10,
                }
            }},
        {'2', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//       
	// ##### 
	// #    #
	//      #
	//      #
	//      #
	//     # 
	//     # 
	//    #  
	//   #   
	//  #    
	// #     
	// ######
	//       
	//       
	//       
	0x06, 0x02, 0x02, 0x02, 0xC2, 0x3C,
    0x18, 0x14, 0x12, 0x11, 0x10, 0x10,
                }
            }},
        {'3', new BitMappedCharacter
            {
            Width = 5,
            Stripes = new byte[]
                {
	//      
	// #### 
	//     #
	//     #
	//     #
	//     #
	//  ### 
	//     #
	//     #
	//     #
	//     #
	//     #
	// #### 
	//      
	//      
	//      
	0x02, 0x42, 0x42, 0x42, 0xBC,
    0x10, 0x10, 0x10, 0x10, 0x0F,
                }
            }},
        {'4', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//      #  
	//     ##  
	//    # #  
	//    # #  
	//   #  #  
	//  #   #  
	//  #   #  
	// #    #  
	// ########
	//      #  
	//      #  
	//      #  
	//         
	//         
	//         
	0x00, 0xC0, 0x20, 0x18, 0x04, 0xFE, 0x00, 0x00,
    0x03, 0x02, 0x02, 0x02, 0x02, 0x1F, 0x02, 0x02,
                }
            }},
        {'5', new BitMappedCharacter
            {
            Width = 5,
            Stripes = new byte[]
                {
	//      
	// #####
	// #    
	// #    
	// #    
	// ###  
	//    # 
	//     #
	//     #
	//     #
	//     #
	//    # 
	// ###  
	//      
	//      
	//      
	0x3E, 0x22, 0x22, 0x42, 0x82,
    0x10, 0x10, 0x10, 0x08, 0x07,
                }
            }},
        {'6', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//   #### 
	//  #     
	//  #     
	// #      
	// # ###  
	// ##   # 
	// #     #
	// #     #
	// #     #
	// #     #
	//  #   # 
	//   ###  
	//        
	//        
	//        
	0xF0, 0x4C, 0x22, 0x22, 0x22, 0x42, 0x80,
    0x07, 0x08, 0x10, 0x10, 0x10, 0x08, 0x07,
                }
            }},
        {'7', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	// #######
	//       #
	//      # 
	//     #  
	//     #  
	//    #   
	//    #   
	//   #    
	//   #    
	//   #    
	//  #     
	//  #     
	//        
	//        
	//        
	0x02, 0x02, 0x02, 0xC2, 0x32, 0x0A, 0x06,
    0x00, 0x18, 0x07, 0x00, 0x00, 0x00, 0x00,
                }
            }},
        {'8', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//  ##### 
	// #     #
	// #     #
	// #     #
	//  #  ## 
	//   ##   
	//  #  ## 
	// #    # 
	// #     #
	// #     #
	// #    # 
	//  ####  
	//        
	//        
	//        
	0x1C, 0xA2, 0x42, 0x42, 0xA2, 0xA2, 0x1C,
    0x0F, 0x10, 0x10, 0x10, 0x10, 0x09, 0x06,
                }
            }},
        {'9', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//   ###  
	//  #   # 
	// #     #
	// #     #
	// #     #
	// #     #
	//  #   ##
	//   ### #
	//       #
	//      # 
	//      # 
	//  ####  
	//        
	//        
	//        
	0x78, 0x84, 0x02, 0x02, 0x02, 0x84, 0xF8,
    0x00, 0x10, 0x11, 0x11, 0x11, 0x0C, 0x03,
                }
            }},
        {':', new BitMappedCharacter
            {
            Width = 2,
            Stripes = new byte[]
                {
	//   
	//   
	//   
	//   
	//   
	// ##
	// ##
	//   
	//   
	//   
	//   
	// ##
	// ##
	//   
	//   
	//   
	0x60, 0x60,
    0x18, 0x18,
                }
            }},
        {';', new BitMappedCharacter
            {
            Width = 2,
            Stripes = new byte[]
                {
	//   
	//   
	//   
	//   
	//   
	// ##
	// ##
	//   
	//   
	//   
	//   
	// ##
	// ##
	//  #
	//  #
	// # 
	0x60, 0x60,
    0x98, 0x78,
                }
            }},
        {'<', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//        
	//        
	//       #
	//     ## 
	//   ##   
	// ##     
	// ##     
	//   ##   
	//     ## 
	//       #
	//        
	//        
	//        
	0x00, 0x00, 0x80, 0x80, 0x40, 0x40, 0x20,
    0x03, 0x03, 0x04, 0x04, 0x08, 0x08, 0x10,
                }
            }},
        {'=', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//         
	//         
	//         
	//         
	//         
	//         
	// ########
	//         
	//         
	// ########
	//         
	//         
	//         
	//         
	//         
	0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04,
                }
            }},
        {'>', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//        
	//        
	// #      
	//  ##    
	//    ##  
	//      ##
	//      ##
	//    ##  
	//  ##    
	// #      
	//        
	//        
	//        
	0x20, 0x40, 0x40, 0x80, 0x80, 0x00, 0x00,
    0x10, 0x08, 0x08, 0x04, 0x04, 0x03, 0x03,
                }
            }},
        {'?', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	// ###### 
	// #    ##
	// #     #
	//       #
	//      # 
	//     #  
	//    #   
	//   #    
	//   #    
	//        
	//   #    
	//   #    
	//        
	//        
	//        
	0x0E, 0x02, 0x02, 0x82, 0x42, 0x26, 0x1C,
    0x00, 0x00, 0x1B, 0x00, 0x00, 0x00, 0x00,
                }
            }},
        {'@', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//    ####   
	//   #    #  
	//  #   ###  
	// ##  #  #  
	// #  #   #  
	// #  #   #  
	// #  #  ##  
	// #  #  ##  
	// #  # ###  
	//  #  ## ###
	//  ##   #   
	//   #####   
	//           
	//           
	//           
	0xF0, 0x18, 0x04, 0xE2, 0x12, 0x0A, 0x8A, 0xFC, 0x00, 0x00,
    0x03, 0x0C, 0x18, 0x13, 0x14, 0x16, 0x1B, 0x07, 0x04, 0x04,
                }
            }},
        {'A', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//           
	//           
	//     #     
	//    ###    
	//    # #    
	//    #  #   
	//   #   #   
	//   #    #  
	//  #######  
	//  #      # 
	//  #      # 
	// #        #
	//           
	//           
	//           
	0x00, 0x00, 0x80, 0x70, 0x18, 0x30, 0xC0, 0x00, 0x00, 0x00,
    0x10, 0x0E, 0x03, 0x02, 0x02, 0x02, 0x02, 0x03, 0x0C, 0x10,
                }
            }},
        {'B', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// ###### 
	// #     #
	// #     #
	// #    # 
	// #####  
	// #    # 
	// #     #
	// #     #
	// #     #
	// ###### 
	//        
	//        
	//        
	0xF8, 0x88, 0x88, 0x88, 0x88, 0x48, 0x30,
    0x1F, 0x10, 0x10, 0x10, 0x10, 0x11, 0x0E,
                }
            }},
        {'C', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//         
	//         
	//    #####
	//  ##    #
	//  #      
	// #       
	// #       
	// #       
	// #       
	//  #      
	//  ##     
	//    #####
	//         
	//         
	//         
	0xC0, 0x30, 0x10, 0x08, 0x08, 0x08, 0x08, 0x18,
    0x03, 0x0C, 0x08, 0x10, 0x10, 0x10, 0x10, 0x10,
                }
            }},
        {'D', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// #####  
	// #    # 
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	// #    # 
	// #####  
	//        
	//        
	//        
	0xF8, 0x08, 0x08, 0x08, 0x08, 0x10, 0xE0,
    0x1F, 0x10, 0x10, 0x10, 0x10, 0x08, 0x07,
                }
            }},
        {'E', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// #######
	// #      
	// #      
	// #      
	// #      
	// ###### 
	// #      
	// #      
	// #      
	// #######
	//        
	//        
	//        
	0xF8, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
    0x1F, 0x11, 0x11, 0x11, 0x11, 0x11, 0x10,
                }
            }},
        {'F', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// #######
	// #      
	// #      
	// #      
	// #      
	// ###### 
	// #      
	// #      
	// #      
	// #      
	//        
	//        
	//        
	0xF8, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08,
    0x1F, 0x01, 0x01, 0x01, 0x01, 0x01, 0x00,
                }
            }},
        {'G', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//         
	//         
	//    #####
	//  ##    #
	//  #      
	// #       
	// #       
	// #    ###
	// #      #
	//  #     #
	//  ##    #
	//    #####
	//         
	//         
	//         
	0xC0, 0x30, 0x10, 0x08, 0x08, 0x08, 0x08, 0x18,
    0x03, 0x0C, 0x08, 0x10, 0x10, 0x11, 0x11, 0x1F,
                }
            }},
        {'H', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	// #######
	// #     #
	// #     #
	// #     #
	// #     #
	//        
	//        
	//        
	0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF8,
    0x1F, 0x01, 0x01, 0x01, 0x01, 0x01, 0x1F,
                }
            }},
        {'I', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// #######
	//    #   
	//    #   
	//    #   
	//    #   
	//    #   
	//    #   
	//    #   
	//    #   
	// #######
	//        
	//        
	//        
	0x08, 0x08, 0x08, 0xF8, 0x08, 0x08, 0x08,
    0x10, 0x10, 0x10, 0x1F, 0x10, 0x10, 0x10,
                }
            }},
        {'J', new BitMappedCharacter
            {
            Width = 5,
            Stripes = new byte[]
                {
	//      
	//      
	//      
	//  ####
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	// #### 
	//      
	//      
	//      
	0x00, 0x08, 0x08, 0x08, 0xF8,
    0x10, 0x10, 0x10, 0x10, 0x0F,
                }
            }},
        {'K', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//         
	//         
	// #     # 
	// #    #  
	// #   #   
	// #  #    
	// ###     
	// # #     
	// #  #    
	// #   ##  
	// #     # 
	// #      #
	//         
	//         
	//         
	0xF8, 0x80, 0x80, 0x40, 0x20, 0x10, 0x08, 0x00,
    0x1F, 0x00, 0x01, 0x02, 0x04, 0x04, 0x08, 0x10,
                }
            }},
        {'L', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// #      
	// #      
	// #      
	// #      
	// #      
	// #      
	// #      
	// #      
	// #      
	// #######
	//        
	//        
	//        
	0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x1F, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10,
                }
            }},
        {'M', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//         
	//         
	// ##    ##
	// ##    ##
	// ###   ##
	// # #  # #
	// # #  # #
	// # ## # #
	// #  ##  #
	// #  ##  #
	// #      #
	// #      #
	//         
	//         
	//         
	0xF8, 0x38, 0xE0, 0x00, 0x00, 0xC0, 0x38, 0xF8,
    0x1F, 0x00, 0x01, 0x07, 0x06, 0x01, 0x00, 0x1F,
                }
            }},
        {'N', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// #     #
	// ##    #
	// # #   #
	// # #   #
	// #  #  #
	// #  #  #
	// #   # #
	// #   # #
	// #    ##
	// #     #
	//        
	//        
	//        
	0xF8, 0x10, 0x60, 0x80, 0x00, 0x00, 0xF8,
    0x1F, 0x00, 0x00, 0x01, 0x06, 0x08, 0x1F,
                }
            }},
        {'O', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//         
	//         
	//   ####  
	//  #    # 
	// #      #
	// #      #
	// #      #
	// #      #
	// #      #
	// #      #
	//  #    # 
	//   ####  
	//         
	//         
	//         
	0xE0, 0x10, 0x08, 0x08, 0x08, 0x08, 0x10, 0xE0,
    0x07, 0x08, 0x10, 0x10, 0x10, 0x10, 0x08, 0x07,
                }
            }},
        {'P', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// ###### 
	// #     #
	// #     #
	// #     #
	// #    # 
	// #####  
	// #      
	// #      
	// #      
	// #      
	//        
	//        
	//        
	0xF8, 0x08, 0x08, 0x08, 0x08, 0x88, 0x70,
    0x1F, 0x01, 0x01, 0x01, 0x01, 0x00, 0x00,
                }
            }},
        {'Q', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//         
	//         
	//   ####  
	//  #    # 
	// #      #
	// #      #
	// #      #
	// #      #
	// #      #
	// #      #
	//  #    # 
	//   ####  
	//      #  
	//       ##
	//         
	0xE0, 0x10, 0x08, 0x08, 0x08, 0x08, 0x10, 0xE0,
    0x07, 0x08, 0x10, 0x10, 0x10, 0x30, 0x48, 0x47,
                }
            }},
        {'R', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// #####  
	// #    # 
	// #    # 
	// #    # 
	// #   #  
	// ####   
	// #  #   
	// #   #  
	// #    # 
	// #     #
	//        
	//        
	//        
	0xF8, 0x08, 0x08, 0x08, 0x88, 0x70, 0x00,
    0x1F, 0x01, 0x01, 0x03, 0x04, 0x08, 0x10,
                }
            }},
        {'S', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//  ######
	// #     #
	// #      
	// ##     
	//   ##   
	//     ## 
	//       #
	//       #
	// #    ##
	// #####  
	//        
	//        
	//        
	0x70, 0x48, 0x88, 0x88, 0x08, 0x08, 0x18,
    0x18, 0x10, 0x10, 0x10, 0x11, 0x09, 0x0E,
                }
            }},
        {'T', new BitMappedCharacter
            {
            Width = 9,
            Stripes = new byte[]
                {
	//          
	//          
	//          
	// #########
	//     #    
	//     #    
	//     #    
	//     #    
	//     #    
	//     #    
	//     #    
	//     #    
	//     #    
	//          
	//          
	//          
	0x08, 0x08, 0x08, 0x08, 0xF8, 0x08, 0x08, 0x08, 0x08,
    0x00, 0x00, 0x00, 0x00, 0x1F, 0x00, 0x00, 0x00, 0x00,
                }
            }},
        {'U', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	//  #   # 
	//  ####  
	//        
	//        
	//        
	0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0xF8,
    0x07, 0x18, 0x10, 0x10, 0x10, 0x08, 0x07,
                }
            }},
        {'V', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//           
	//           
	// #        #
	//  #      # 
	//  #      # 
	//   #    #  
	//   #    #  
	//   #   #   
	//    #  #   
	//    #  #   
	//     ##    
	//     ##    
	//           
	//           
	//           
	0x08, 0x30, 0xC0, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x30, 0x08,
    0x00, 0x00, 0x01, 0x06, 0x18, 0x18, 0x07, 0x00, 0x00, 0x00,
                }
            }},
        {'W', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//           
	//           
	// #        #
	// #        #
	//  #  #   # 
	//  #  ##  # 
	//  #  ##  # 
	//  # # #  # 
	//  # #  # # 
	//   ##  ##  
	//   ##  ##  
	//   #    #  
	//           
	//           
	//           
	0x18, 0xE0, 0x00, 0x00, 0xE0, 0xC0, 0x00, 0x00, 0xE0, 0x18,
    0x00, 0x03, 0x1C, 0x0F, 0x00, 0x01, 0x0E, 0x1C, 0x03, 0x00,
                }
            }},
        {'X', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//           
	//           
	// #        #
	//  #      # 
	//   #    #  
	//    #  #   
	//     ##    
	//     ##    
	//    #  #   
	//   #    #  
	//  #      # 
	// #        #
	//           
	//           
	//           
	0x08, 0x10, 0x20, 0x40, 0x80, 0x80, 0x40, 0x20, 0x10, 0x08,
    0x10, 0x08, 0x04, 0x02, 0x01, 0x01, 0x02, 0x04, 0x08, 0x10,
                }
            }},
        {'Y', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//           
	//           
	// #        #
	//  #      # 
	//   #    #  
	//   #   #   
	//    # #    
	//     #     
	//     #     
	//     #     
	//     #     
	//     #     
	//           
	//           
	//           
	0x08, 0x10, 0x60, 0x80, 0x00, 0x80, 0x40, 0x20, 0x10, 0x08,
    0x00, 0x00, 0x00, 0x00, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00,
                }
            }},
        {'Z', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//         
	//         
	// ########
	//        #
	//       # 
	//      #  
	//     #   
	//    #    
	//   #     
	//  #      
	// #       
	// ########
	//         
	//         
	//         
	0x08, 0x08, 0x08, 0x08, 0x88, 0x48, 0x28, 0x18,
    0x18, 0x14, 0x12, 0x11, 0x10, 0x10, 0x10, 0x10,
                }
            }},
        {'[', new BitMappedCharacter
            {
            Width = 5,
            Stripes = new byte[]
                {
	//      
	// #####
	// #    
	// #    
	// #    
	// #    
	// #    
	// #    
	// #    
	// #    
	// #    
	// #    
	// #    
	// #    
	// #####
	//      
	0xFE, 0x02, 0x02, 0x02, 0x02,
    0x7F, 0x40, 0x40, 0x40, 0x40,
                }
            }},
        {'\\', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	// #       
	//  #      
	//  #      
	//   #     
	//   #     
	//    #    
	//    #    
	//     #   
	//     #   
	//      #  
	//      #  
	//       # 
	//       # 
	//        #
	//         
	0x02, 0x0C, 0x30, 0xC0, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x03, 0x0C, 0x30, 0x40,
                }
            }},
        {']', new BitMappedCharacter
            {
            Width = 5,
            Stripes = new byte[]
                {
	//      
	// #####
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	// #####
	//      
	0x02, 0x02, 0x02, 0x02, 0xFE,
    0x40, 0x40, 0x40, 0x40, 0x7F,
                }
            }},
        {'^', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//     #   
	//     #   
	//    ##   
	//    # #  
	//   ## #  
	//   #  #  
	//   #   # 
	//  #    # 
	//  #    # 
	// #      #
	//         
	//         
	//         
	//         
	//         
	0x00, 0x00, 0xE0, 0x38, 0x0E, 0x70, 0x80, 0x00,
    0x04, 0x03, 0x00, 0x00, 0x00, 0x00, 0x03, 0x04,
                }
            }},
        {'_', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//           
	//           
	//           
	//           
	//           
	//           
	//           
	//           
	//           
	//           
	//           
	//           
	// ##########
	//           
	//           
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20,
                }
            }},
        {'`', new BitMappedCharacter
            {
            Width = 2,
            Stripes = new byte[]
                {
	// # 
	//  #
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	//   
	0x01, 0x02,
    0x00, 0x00,
                }
            }},
        {'a', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//        
	//        
	//  ####  
	//      # 
	//      # 
	//   #### 
	//  #   # 
	// #    # 
	// #   ## 
	//  ### ##
	//        
	//        
	//        
	0x00, 0x20, 0x20, 0x20, 0x20, 0xC0, 0x00,
    0x0C, 0x12, 0x11, 0x11, 0x09, 0x1F, 0x10,
                }
            }},
        {'b', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	// #      
	// #      
	// #      
	// #      
	// # #### 
	// ##   # 
	// #     #
	// #     #
	// #     #
	// #     #
	// ##   # 
	// # ###  
	//        
	//        
	//        
	0xFE, 0x40, 0x20, 0x20, 0x20, 0x60, 0x80,
    0x1F, 0x08, 0x10, 0x10, 0x10, 0x08, 0x07,
                }
            }},
        {'c', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//        
	//        
	//   #####
	//  #     
	// #      
	// #      
	// #      
	// #      
	//  #     
	//   #####
	//        
	//        
	//        
	0x80, 0x40, 0x20, 0x20, 0x20, 0x20, 0x20,
    0x07, 0x08, 0x10, 0x10, 0x10, 0x10, 0x10,
                }
            }},
        {'d', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//       #
	//       #
	//       #
	//       #
	//   ### #
	//  #   ##
	// #     #
	// #     #
	// #     #
	// #     #
	//  #   ##
	//  #### #
	//        
	//        
	//        
	0x80, 0x40, 0x20, 0x20, 0x20, 0x40, 0xFE,
    0x07, 0x18, 0x10, 0x10, 0x10, 0x08, 0x1F,
                }
            }},
        {'e', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//        
	//        
	//   #### 
	//  #    #
	// #     #
	// #######
	// #      
	// #      
	//  #     
	//   #####
	//        
	//        
	//        
	0x80, 0x40, 0x20, 0x20, 0x20, 0x20, 0xC0,
    0x07, 0x09, 0x11, 0x11, 0x11, 0x11, 0x11,
                }
            }},
        {'f', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//     ####
	//   ##    
	//   #     
	//   #     
	// ########
	//   #     
	//   #     
	//   #     
	//   #     
	//   #     
	//   #     
	//   #     
	//         
	//         
	//         
	0x20, 0x20, 0xFC, 0x24, 0x22, 0x22, 0x22, 0x22,
    0x00, 0x00, 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00,
                }
            }},
        {'g', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//        
	//        
	//   ### #
	//  #   ##
	// #     #
	// #     #
	// #     #
	// #     #
	//  #   ##
	//  #### #
	//       #
	//      # 
	//  ####  
	0x80, 0x40, 0x20, 0x20, 0x20, 0x40, 0xE0,
    0x07, 0x98, 0x90, 0x90, 0x90, 0x48, 0x3F,
                }
            }},
        {'h', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	// #      
	// #      
	// #      
	// #      
	// #  ### 
	// # #   #
	// ##    #
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	//        
	//        
	//        
	0xFE, 0x80, 0x40, 0x20, 0x20, 0x20, 0xC0,
    0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F,
                }
            }},
        {'i', new BitMappedCharacter
            {
            Width = 4,
            Stripes = new byte[]
                {
	//     
	//   ##
	//   ##
	//     
	//     
	// ####
	//    #
	//    #
	//    #
	//    #
	//    #
	//    #
	//    #
	//     
	//     
	//     
	0x20, 0x20, 0x26, 0xE6,
    0x00, 0x00, 0x00, 0x1F,
                }
            }},
        {'j', new BitMappedCharacter
            {
            Width = 5,
            Stripes = new byte[]
                {
	//      
	//    ##
	//    ##
	//      
	//      
	//  ####
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	//     #
	// #### 
	0x00, 0x20, 0x20, 0x26, 0xE6,
    0x80, 0x80, 0x80, 0x80, 0x7F,
                }
            }},
        {'k', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	// #      
	// #      
	// #      
	// #      
	// #     #
	// #   ## 
	// #  #   
	// ###    
	// # #    
	// #  ##  
	// #    # 
	// #     #
	//        
	//        
	//        
	0xFE, 0x00, 0x00, 0x80, 0x40, 0x40, 0x20,
    0x1F, 0x01, 0x03, 0x04, 0x04, 0x08, 0x10,
                }
            }},
        {'l', new BitMappedCharacter
            {
            Width = 4,
            Stripes = new byte[]
                {
	//     
	// ####
	//    #
	//    #
	//    #
	//    #
	//    #
	//    #
	//    #
	//    #
	//    #
	//    #
	//    #
	//     
	//     
	//     
	0x02, 0x02, 0x02, 0xFE,
    0x00, 0x00, 0x00, 0x1F,
                }
            }},
        {'m', new BitMappedCharacter
            {
            Width = 9,
            Stripes = new byte[]
                {
	//          
	//          
	//          
	//          
	//          
	// # ##  ## 
	// ##  ##  #
	// #   #   #
	// #   #   #
	// #   #   #
	// #   #   #
	// #   #   #
	// #   #   #
	//          
	//          
	//          
	0xE0, 0x40, 0x20, 0x20, 0xC0, 0x40, 0x20, 0x20, 0xC0,
    0x1F, 0x00, 0x00, 0x00, 0x1F, 0x00, 0x00, 0x00, 0x1F,
                }
            }},
        {'n', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//        
	//        
	// #  ### 
	// ###   #
	// ##    #
	// #     #
	// #     #
	// #     #
	// #     #
	// #     #
	//        
	//        
	//        
	0xE0, 0xC0, 0x40, 0x20, 0x20, 0x20, 0xC0,
    0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F,
                }
            }},
        {'o', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//        
	//        
	//   ###  
	//  #   # 
	// #     #
	// #     #
	// #     #
	// #     #
	//  #   # 
	//   ###  
	//        
	//        
	//        
	0x80, 0x40, 0x20, 0x20, 0x20, 0x40, 0x80,
    0x07, 0x08, 0x10, 0x10, 0x10, 0x08, 0x07,
                }
            }},
        {'p', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//        
	//        
	// # #### 
	// ##   # 
	// #     #
	// #     #
	// #     #
	// #     #
	// ##   # 
	// # ###  
	// #      
	// #      
	// #      
	0xE0, 0x40, 0x20, 0x20, 0x20, 0x60, 0x80,
    0xFF, 0x08, 0x10, 0x10, 0x10, 0x08, 0x07,
                }
            }},
        {'q', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//        
	//        
	//   ### #
	//  #   ##
	// #     #
	// #     #
	// #     #
	// #     #
	//  #   ##
	//  #### #
	//       #
	//       #
	//       #
	0x80, 0x40, 0x20, 0x20, 0x20, 0x40, 0xE0,
    0x07, 0x18, 0x10, 0x10, 0x10, 0x08, 0xFF,
                }
            }},
        {'r', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//       
	//       
	//       
	//       
	//       
	// #  ###
	// # #  #
	// ##   #
	// #     
	// #     
	// #     
	// #     
	// #     
	//       
	//       
	//       
	0xE0, 0x80, 0x40, 0x20, 0x20, 0xE0,
    0x1F, 0x00, 0x00, 0x00, 0x00, 0x00,
                }
            }},
        {'s', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//       
	//       
	//       
	//       
	//       
	//  #####
	// #     
	// #     
	//  ##   
	//    ## 
	//      #
	// #    #
	// ##### 
	//       
	//       
	//       
	0xC0, 0x20, 0x20, 0x20, 0x20, 0x20,
    0x18, 0x11, 0x11, 0x12, 0x12, 0x0C,
                }
            }},
        {'t', new BitMappedCharacter
            {
            Width = 7,
            Stripes = new byte[]
                {
	//        
	//        
	//        
	//   #    
	//   #    
	// #######
	//   #    
	//   #    
	//   #    
	//   #    
	//   #    
	//   #    
	//    ####
	//        
	//        
	//        
	0x20, 0x20, 0xF8, 0x20, 0x20, 0x20, 0x20,
    0x00, 0x00, 0x0F, 0x10, 0x10, 0x10, 0x10,
                }
            }},
        {'u', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//       
	//       
	//       
	//       
	//       
	// #    #
	// #    #
	// #    #
	// #    #
	// #    #
	// #    #
	// #   ##
	//  ### #
	//       
	//       
	//       
	0xE0, 0x00, 0x00, 0x00, 0x00, 0xE0,
    0x0F, 0x10, 0x10, 0x10, 0x08, 0x1F,
                }
            }},
        {'v', new BitMappedCharacter
            {
            Width = 9,
            Stripes = new byte[]
                {
	//          
	//          
	//          
	//          
	//          
	// #       #
	//  #     # 
	//  #     # 
	//  #    #  
	//   #   #  
	//   #  #   
	//    # #   
	//    ##    
	//          
	//          
	//          
	0x20, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x20,
    0x00, 0x01, 0x06, 0x18, 0x10, 0x0C, 0x03, 0x00, 0x00,
                }
            }},
        {'w', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//           
	//           
	//           
	//           
	// #        #
	// #   #    #
	//  #  ##  # 
	//  #  ##  # 
	//  # #  # # 
	//  # #  # # 
	//   ##  ##  
	//   #    #  
	//           
	//           
	//           
	0x60, 0x80, 0x00, 0x00, 0xC0, 0x80, 0x00, 0x00, 0x80, 0x60,
    0x00, 0x07, 0x18, 0x0E, 0x01, 0x01, 0x0E, 0x18, 0x07, 0x00,
                }
            }},
        {'x', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//         
	//         
	//         
	//         
	// #      #
	//  #    # 
	//   #  #  
	//    ##   
	//    ##   
	//   #  #  
	//  #    # 
	// #      #
	//         
	//         
	//         
	0x20, 0x40, 0x80, 0x00, 0x00, 0x80, 0x40, 0x20,
    0x10, 0x08, 0x04, 0x03, 0x03, 0x04, 0x08, 0x10,
                }
            }},
        {'y', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//           
	//           
	//           
	//           
	// #        #
	//  #      # 
	//  #      # 
	//   #    #  
	//   ##  #   
	//    #  #   
	//     ##    
	//     ##    
	//     #     
	//    #      
	// ###       
	0x20, 0xC0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0, 0x20,
    0x80, 0x80, 0x83, 0x46, 0x38, 0x18, 0x06, 0x01, 0x00, 0x00,
                }
            }},
        {'z', new BitMappedCharacter
            {
            Width = 8,
            Stripes = new byte[]
                {
	//         
	//         
	//         
	//         
	//         
	// ########
	//       # 
	//      #  
	//     #   
	//    #    
	//   #     
	//  #      
	// ########
	//         
	//         
	//         
	0x20, 0x20, 0x20, 0x20, 0x20, 0xA0, 0x60, 0x20,
    0x10, 0x18, 0x14, 0x12, 0x11, 0x10, 0x10, 0x10,
                }
            }},
        {'{', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//       
	//    ###
	//   #   
	//   #   
	//   #   
	//   #   
	//   #   
	//   #   
	// ##    
	//   #   
	//   #   
	//   #   
	//   #   
	//   #   
	//    ###
	//       
	0x00, 0x00, 0xFC, 0x02, 0x02, 0x02,
    0x01, 0x01, 0x3E, 0x40, 0x40, 0x40,
                }
            }},
        {'|', new BitMappedCharacter
            {
            Width = 1,
            Stripes = new byte[]
                {
	//  
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	// #
	//  
	0xFE,
    0x7F,
                }
            }},
        {'}', new BitMappedCharacter
            {
            Width = 6,
            Stripes = new byte[]
                {
	//       
	// ###   
	//    #  
	//    #  
	//    #  
	//    #  
	//    #  
	//    #  
	//     ##
	//    #  
	//    #  
	//    #  
	//    #  
	//    #  
	// ###   
	//       
	0x02, 0x02, 0x02, 0xFC, 0x00, 0x00,
    0x40, 0x40, 0x40, 0x3E, 0x01, 0x01,
                }
            }},
        {'~', new BitMappedCharacter
            {
            Width = 10,
            Stripes = new byte[]
                {
	//           
	//           
	//           
	//           
	//           
	//           
	//           
	//  ###     #
	// #   ##   #
	// #     ### 
	//           
	//           
	//           
	//           
	//           
	//           
	0x00, 0x80, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80,
    0x03, 0x00, 0x00, 0x00, 0x01, 0x01, 0x02, 0x02, 0x02, 0x01,
                }
            }},
        };
            }
        }

    }
