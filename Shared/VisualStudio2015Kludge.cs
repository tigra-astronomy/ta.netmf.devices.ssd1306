using System;
using Microsoft.SPOT;
using TA.NetMF.Utilities;

// ReSharper disable once CheckNamespace
namespace System.Diagnostics
    {
    public enum DebuggerBrowsableState
        {
        Never,
        Collapsed,
        RootHidden
        }
    }