// This file is part of the TA.NetMF.SSD1306 project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: DisplayTransactionBuilder.cs  Last modified: 2015-09-11@05:55 by Tim Long

using TA.NetMF.Utilities;
using TA.NetMF.Utilities.I2C;

namespace TA.NetMF.Devices.Ssd1306
    {
    /// <summary>
    ///     Class DisplayTransactionBuilder. A class for building I2C display transactions.
    ///     Includes a set of methods corresponding to each command code, with
    ///     default parameters that correspond to power-on settings.
    /// </summary>
    [UsedImplicitly]
    public sealed class DisplayTransactionBuilder : TransactionBuilder
        {
        const byte CommandModeControlByte = 0x00;
        const byte DataModeControlByte = 0x40;


        /// <summary>
        ///     Prevents a default instance of the <see cref="DisplayTransactionBuilder" /> class from being created.
        /// </summary>
        DisplayTransactionBuilder() {}


        /// <summary>
        ///     Creates a transaction builder that builds a command mode transaction.
        ///     The entire transaction will be treated as a stream of commands.
        /// </summary>
        /// <returns>DisplayTransactionBuilder.</returns>
        public static IWriteTransactionBuilder CommandModeTransaction()
            {
            return WriteTransaction().Append(CommandModeControlByte);
            }

        /// <summary>
        ///     Creates a transaction builder that builds a data mode transaction.
        ///     All of the data from the transaction will be written to the CGRAM buffer and treated as pixel
        ///     bitmap data for the display.
        /// </summary>
        /// <returns>DisplayTransactionBuilder.</returns>
        public static IWriteTransactionBuilder DataModeTransaction()
            {
            return WriteTransaction().Append(DataModeControlByte);
            }
        }
    }