// This file is part of the TA.NetMF.SSD1306 project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: TransactionBuilderExtensions.cs  Last modified: 2015-09-11@05:55 by Tim Long

using System;
using TA.NetMF.Utilities;
using TA.NetMF.Utilities.I2C;

namespace TA.NetMF.Devices.Ssd1306
    {
    [UsedImplicitly(ImplicitUseTargetFlags.Members)]
    public static class TransactionBuilderExtensions
        {
        const string AllowedPageRange = "range 0..7 inclusive";
        const string EndBeforeStart = "End must be >= Start";
        const string AllowedColumnRange = "Range 0..127 inclusive";
        const string Range0To63 = "Range 0..63 inclusive";
        const string Range0to3 = "Range 0..3 inclusive";
        const string Range1to15 = "Range 1..15 inclusive";

        static readonly byte[] blankPage =
            {
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
            };

        /// <summary>
        ///     Appends exactly one memory page of blanks (0x00).
        /// </summary>
        /// <returns><see cref="DisplayTransactionBuilder" />.</returns>
        public static IWriteTransactionBuilder BlankPage(this IWriteTransactionBuilder builder)
            {
            return builder.Append(blankPage);
            }

        /// <summary>
        ///     Configures the display to write to the defined area.
        /// </summary>
        /// <param name="builder">An instance of <see cref="IWriteTransactionBuilder" /></param>
        /// <param name="pageStart">The page start number [0..7].</param>
        /// <param name="pageEnd">The inclusive page end number [0..7].</param>
        /// <param name="columnStart">The column start number [0..127].</param>
        /// <param name="columnEnd">The inclusive column end number [0..127].</param>
        /// <exception cref="ArgumentOutOfRangeException">
        ///     Thrown if the specified coordinates are off the screen or if the end
        ///     coordinate is not greater than the start coordinate.
        /// </exception>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder SetWriteWindow(this IWriteTransactionBuilder builder, int pageStart, int pageEnd, int columnStart, int columnEnd)
            {
            if (pageStart > 7 || pageStart < 0) throw new ArgumentOutOfRangeException("pageStart", AllowedPageRange);
            if (pageEnd > 7 || pageEnd < 0) throw new ArgumentOutOfRangeException("pageEnd", AllowedPageRange);
            if (columnStart > 127 || columnStart < 0)
                throw new ArgumentOutOfRangeException("columnStart", AllowedColumnRange);
            if (columnEnd > 127 || columnEnd < 0)
                throw new ArgumentOutOfRangeException("columnEnd", AllowedColumnRange);
            if (pageEnd < pageStart) throw new ArgumentOutOfRangeException("pageEnd", EndBeforeStart);
            if (columnEnd < columnStart) throw new ArgumentOutOfRangeException("columnEnd", EndBeforeStart);
            builder.Append(0x21, columnStart, columnEnd);
            builder.Append(0x22, pageStart, pageEnd);
            return builder;
            }

        /// <summary>
        ///     Sets the display to use Paged Addressing Mode.
        /// </summary>
        /// <param name="builder">An instance of <see cref="IWriteTransactionBuilder" /></param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder PageAddressingMode(this IWriteTransactionBuilder builder)
            {
            return builder.Append(0x20, 0x02);
            }

        /// <summary>
        ///     Sets the display to use Horizontal Addressing Mode.
        /// </summary>
        /// <param name="builder">An instance of <see cref="IWriteTransactionBuilder" /></param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder HorizontalAddressingMode(this IWriteTransactionBuilder builder)
            {
            return builder.Append(0x20, 0x00);
            }

        /// <summary>
        ///     Sets the display to use Vertical Addressing Mode.
        /// </summary>
        /// <param name="builder">An instance of <see cref="IWriteTransactionBuilder" /></param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder VerticalAddressingMode(this IWriteTransactionBuilder builder)
            {
            return builder.Append(0x20, 0x01);
            }

        /// <summary>
        ///     Sets the page and column start pointers when in Paged Addressing Mode.
        ///     Do not use this setting for Horizontal or Vertical addressing mode.
        /// </summary>
        /// <param name="builder">An instance of <see cref="IWriteTransactionBuilder" /></param>
        /// <param name="page">The page.</param>
        /// <param name="column">The column.</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder PageModePageAndColumnPointer(this IWriteTransactionBuilder builder, byte page, byte column)
            {
            return builder.PageModePagePointer(page).PageModeColumnPointer(column);
            }

        /// <summary>
        ///     Sets the page address pointer when in Paged Addressing Mode.
        ///     Do not use this setting for Horizontal or Vertical addressing mode.
        /// </summary>
        /// <param name="builder">An instance of <see cref="IWriteTransactionBuilder" /></param>
        /// <param name="page">The page address pointer.</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the specified page is out of range. </exception>
        public static IWriteTransactionBuilder PageModePagePointer(this IWriteTransactionBuilder builder, byte page)
            {
            if (page > 7) throw new ArgumentOutOfRangeException(AllowedPageRange);
            return builder.Append(0xB0 | page);
            }

        /// <summary>
        ///     Sets the column address pointer when in Paged Addressing Mode.
        ///     Do not use this setting for Horizontal or Vertical addressing mode.
        /// </summary>
        /// <param name="builder">An instance of <see cref="IWriteTransactionBuilder" /></param>
        /// <param name="column">The column address pointer.</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if the specified page is out of range. </exception>
        public static IWriteTransactionBuilder PageModeColumnPointer(this IWriteTransactionBuilder builder, byte column)
            {
            if (column > 127) throw new ArgumentOutOfRangeException(AllowedColumnRange);
            var colLowNybble = column & 0x0F;
            var colHighNybble = column >> 4;
            return builder.Append(colLowNybble).Append(0x10 | colHighNybble);
            }

        /// <summary>
        ///     Controls low-power sleep mode. When asleep, the display appears to be powered off, but still responds to commands.
        /// </summary>
        /// <param name="builder">An instance of <see cref="IWriteTransactionBuilder" /></param>
        /// <param name="sleep">if set to <c>true</c> puts the display to sleep; otherwise wakes it up.</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder SleepMode(this IWriteTransactionBuilder builder, bool sleep)
            {
            return builder.Append(sleep ? 0xAE : 0xAF);
            }

        /// <summary>
        ///     Configure the clock divider ratio and oscillator frequency.
        /// </summary>
        /// <param name="builder">An instance of <see cref="IWriteTransactionBuilder" /></param>
        /// <param name="dividerRatio">The divider ratio.</param>
        /// <param name="oscillatorFrequency">The oscillator frequency.</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder ConfigureClockDivider(this IWriteTransactionBuilder builder, byte dividerRatio = 0, byte oscillatorFrequency = 0)
            {
            //ToDo: Better documentation needed and parameter validation
            var parameter = (byte) ((oscillatorFrequency << 4) | (dividerRatio & 0x0F));
            return builder.Append(0xD5).Append(parameter);
            }

        /// <summary>
        ///     Configures the display multiplex ratio.
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="ratio">The ratio, from 16 to 64 inclusive.</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">ratio;Range 16..64</exception>
        public static IWriteTransactionBuilder MultiplexRatio(this IWriteTransactionBuilder builder, byte ratio = 64)
            {
            if (ratio < 16 || ratio > 64) throw new ArgumentOutOfRangeException("ratio", "Range 16..64 inclusive");
            return builder.Append(0xA8).Append(ratio - 1);
            }

        /// <summary>
        ///     Sets the display offset.
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="offset">The offset, in range 0 to 63 includive; optional, default=0.</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown if offset is out of range.</exception>
        public static IWriteTransactionBuilder DisplayOffset(this IWriteTransactionBuilder builder, byte offset = 0)
            {
            if (offset > 63) throw new ArgumentOutOfRangeException("offset", Range0To63);
            return builder.Append(0xD3).Append(offset);
            }

        /// <summary>
        ///     Displays the start line.
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="line">The display start line [0..63].</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">line</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if line is not in range 0 to 63</exception>
        public static IWriteTransactionBuilder DisplayStartLine(this IWriteTransactionBuilder builder, byte line = 0)
            {
            if (line > 63) throw new ArgumentOutOfRangeException("line", Range0To63);
            return builder.Append(0x40 | line);
            }

        /// <summary>
        ///     Enables or disables the charge pump, which enables the display to generate its own voltages. Many
        ///     typical display modules have only a 3.3 Volt supply and in that case, the charge pump should be enabled.
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="enabled">
        ///     if set to <c>true</c> then the charge pump is enabled; otherwise it is disabled. Optional; default=true
        /// </param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder ChargePump(this IWriteTransactionBuilder builder, bool enabled = true)
            {
            return builder.Append(0x8D, enabled ? 0x14 : 0x10);
            }

        /// <summary>
        ///     Configures segment remapping.
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="remapped">
        ///     if set to <c>true</c> then the segment order is remapped from 127 to 0 and the
        ///     display is flipped left-to-right; otherwise the normal mapping 0 to 127 is used.
        /// </param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder SegmentRemap(this IWriteTransactionBuilder builder, bool remapped = false)
            {
            return builder.Append(remapped ? 0xA1 : 0xA0);
            }

        /// <summary>
        ///     Configures the Common pin scan direction
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="remapped">
        ///     if set to <c>true</c> then the scan direction is reversed and the display is flipped
        ///     top-to-bottom; otherwise teh normal scan direction is used; optional, default=false.
        /// </param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder CommonScanDirection(this IWriteTransactionBuilder builder, bool remapped = false)
            {
            return builder.Append(remapped ? 0xC8 : 0xC0);
            }

        /// <summary>
        ///     Sets the configuration of the Common outputs, allowing flexibility in OLED panel hardware layout.
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="interleaved">
        ///     if set to <c>true</c> then the common pins are interleaved; otherwise they are sequential; optional,
        ///     default=true.
        /// </param>
        /// <param name="remapped">
        ///     if set to <c>true</c> then the common pins are remapped left-to-right and the display top and bottom
        ///     halves are exchanged.
        /// </param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        /// <remarks>
        ///     <para>
        ///         The SSD1306 has 64 common outputs which are arranged in two banks. Rows 0..31 are on the right side of
        ///         the device and rows 32..63 are on the left side of the device. Each common drives one row of pixels on
        ///         the display and there are two distinct approaches to connecting them. One approach takes all the commons
        ///         from one side of the device and connecting them sequentially to one half of the display (normally the
        ///         right side drives the lower half of the display and the left side drives the upper half of the display).
        ///         Alternatively, the commons can be 'interleaved', in which case alternate rows from the left and right
        ///         side are connected to sequential rows of the display. In either case, the common outputs can be
        ///         'remapped' which means the left and right banks are logically reversed. The SSD1306 allows configuration
        ///         of whether the commons are interleaved or sequential; and which bank (left or tight) comes first (i.e.
        ///         remapped left-to-right).
        ///     </para>
        ///     <para>
        ///         We suggest starting with the default values. If your display has the top half at the bottom and the bottom
        ///         half at the top, or if every pair of lines is swapped odd for even, change the value of remapped. if the
        ///         display appears to have the top and bottom halves superimposed, change the value of interleaved.
        ///     </para>
        ///     <para>
        ///         Please refer to section 10.1.18 of the data sheet for more information.
        ///     </para>
        /// </remarks>
        public static IWriteTransactionBuilder CommonOutputConfiguration(this IWriteTransactionBuilder builder, bool interleaved = true, bool remapped = false)
            {
            var parameter = 0x02 | (interleaved ? 0x10 : 0) | (remapped ? 0x20 : 0);
            return builder.Append(0xDA, parameter);
            }

        /// <summary>
        ///     Sets the display contrast in 256 steps.
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="contrast">The contrast setting, range 0..255.</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder DisplayContrast(this IWriteTransactionBuilder builder, byte contrast = 0x7f)
            {
            return builder.Append(0x81).Append(contrast);
            }

        /// <summary>
        ///     Configures the OLED pre-charge period.
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="phase1">The phase1 duration, in multiples of DCLK; optional, default=2.</param>
        /// <param name="phase2">The phase2 duration, in multiples od DCLK; optional, default=2.</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">phase1 or phase2 > 15</exception>
        public static IWriteTransactionBuilder PrechargePeriod(this IWriteTransactionBuilder builder, byte phase1 = 2, byte phase2 = 2)
            {
            if (phase1 < 1 || phase1 > 15) throw new ArgumentOutOfRangeException("phase1", Range1to15);
            if (phase2 < 1 || phase2 > 15) throw new ArgumentOutOfRangeException("phase2", Range1to15);
            var parameter = (phase2 << 4) | phase1;
            return builder.Append(0xD9, parameter);
            }

        /// <summary>
        ///     Sets the Vcomh Deselect Level (adjust the Vcomh regulator output).
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="level">The Vcomh regulator output level; optional, default=2.</param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder VcomhDeselectLevel(this IWriteTransactionBuilder builder, byte level = 2)
            {
            return builder.Append(0xDB, level << 4);
            }

        /// <summary>
        ///     Enables or disables Test Mode (all pixels on).
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="enabled">
        ///     if set to <c>true</c> then all display pixels are forced on regardless of CGRAM
        ///     contents. If set to <c>false</c> then the display resumes normal operation.
        /// </param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder TestMode(this IWriteTransactionBuilder builder, bool enabled = false)
            {
            return builder.Append(enabled ? 0xA5 : 0xA4);
            }

        /// <summary>
        ///     Sets the display polarity (normal or inverse).
        /// </summary>
        /// <param name="builder">An instance of <see cref="TA.NetMF.Utilities.I2C.IWriteTransactionBuilder" /></param>
        /// <param name="positive">
        ///     if set to <c>true</c> then setting a bit to 1 in CGRAM will illuminate the
        ///     corresponding display pixel; otherwise setting a bit to 0 in CGRAM will illuminate the pixel..
        /// </param>
        /// <returns>The modified IWriteTransactionBuilder instance.</returns>
        public static IWriteTransactionBuilder DisplayPolarity(this IWriteTransactionBuilder builder, bool positive = true)
            {
            return builder.Append(positive ? 0xA6 : 0xA7);
            }
        }
    }