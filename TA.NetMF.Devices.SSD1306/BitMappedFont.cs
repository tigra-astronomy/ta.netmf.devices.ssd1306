// This file is part of the TA.NetMF.SSD1306 project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: BitMappedFont.cs  Last modified: 2015-10-19@04:38 by Tim Long

using System.Collections;

namespace TA.NetMF.Devices.Ssd1306
    {
    /// <summary>
    ///     Class BitMappedFont. This <see langword="abstract" /> class contains most of the logic needed to manipulate fonts
    ///     but has no font bitmap data of its own, therefore it must be sub-classed to be of any use.
    /// </summary>
    public abstract class BitMappedFont
        {
        const char Space = ' '; // Every font must contain space.

        protected IDictionary Characters { get; set; }

        /// <summary>
        ///     Gets the height of the character cell, in display pages.
        ///     Fonts are always an even multiple of display pages in height.
        ///     Each display page (or 'stripe') is 8 pixels tall.
        /// </summary>
        /// <value>The height of the character cell, in display pages.</value>
        public int CellPageHeight { get; protected set; }

        /// <summary>
        ///     Gets the amount of space that should be used between characters when rendering the font.
        /// </summary>
        public byte Tracking { get; protected set; } = 1;


        /// <summary>
        ///     Gets the bitmap structure for a given character, or space of the character is not present in the font.
        /// </summary>
        /// <param name="character">The character.</param>
        /// <returns>BitMappedCharacter.</returns>
        public BitMappedCharacter GetBitmap(char character)
            {
            if (Characters.Contains(character))
                {
                return Characters[character] as BitMappedCharacter;
                }
            return Characters[' '] as BitMappedCharacter;
            }
        }

    /// <summary>
    ///     Class BitMappedCharacter. Represents the display stripes and columns that make up a single character.
    ///     Data is arranged in a grid, in Row Major order. Each byte of data represents a vertical column of 8 pixels
    ///     on the display, with the MSB at the bottom. The 8-pixel colums are arranged in 'stripes' across the display.
    ///     This arrangement is designed to optimize the ease of writing to the display over the I2C bus, because an entire
    ///     stripe can be written in a single transaction.
    /// </summary>
    public class BitMappedCharacter
        {
        /// <summary>
        ///     Gets the width of the character in pixels.
        /// </summary>
        /// <value>The height.</value>
        public int Width { get; set; }

        /// <summary>
        ///     Gets the stripes of data for the character bitmap in one contiguous stream of bytes.
        /// </summary>
        /// <value>The stripes.</value>
        public byte[] Stripes { get; set; }
        }
    }