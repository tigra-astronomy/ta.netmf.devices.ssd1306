// This file is part of the TA.NetMF.SSD1306 project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: AssemblyInfo.cs  Last modified: 2015-09-11@05:55 by Tim Long

using System.Reflection;

[assembly: AssemblyTitle("SSD1306 I2C Display Driver for Netduino Plus")]
[assembly: AssemblyDescription("A text-based display driver for SSD1306 based OLED displays, with I2C interface")]

// Version number and other attributes are in GlobalAssemblyInfo.cs