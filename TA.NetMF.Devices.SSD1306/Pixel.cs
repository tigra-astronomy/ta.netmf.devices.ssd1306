// This file is part of the TA.NetMF.SSD1306 project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: Pixel.cs  Last modified: 2015-09-11@05:55 by Tim Long

using Microsoft.SPOT.Hardware;

namespace TA.NetMF.Devices.Ssd1306
    {
    internal sealed class Pixel
        {
        public static int XSize = 128;
        public static int YSize = 64;
        static int Pages = YSize / 8;

        public ushort Page { get; set; }

        public ushort Segment { get; set; }

        public ushort Bit { get; set; }

        public Pixel FromXYCoordinate(int x, int y)
            {
            var p = new Pixel();
            p.Page = (ushort) (y / 8);
            p.Segment = (ushort) x;
            p.Bit = (ushort) (7 - y % 8);
            return p;
            }

        public void Paint(I2CDevice iic) {}
        }
    }