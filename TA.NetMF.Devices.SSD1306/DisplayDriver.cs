// This file is part of the TA.NetMF.SSD1306 project
// 
// Copyright � 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: DisplayDriver.cs  Last modified: 2015-10-19@04:41 by Tim Long

using System;
using System.IO;
using Microsoft.SPOT.Hardware;
using TA.NetMF.Utilities.I2C;

namespace TA.NetMF.Devices.Ssd1306
    {
    public class DisplayDriver
        {
        readonly ThreadSafeI2CDevice iicDevice;
        static readonly I2CDevice.I2CTransaction[] InitializeTransaction =
            {
            I2CDevice.CreateWriteTransaction(new Byte[]
                {
                //CommandModeTransaction,
                0xAE, // Sleep mode
                0xD5, 0x80, // Display Clock divider ratio/Oscillator frequency (=default)
                0xA8, 0x3F, // Multiplex
                0xD3, 0x00, // Display Offset (none)
                0x40, // Set Display Start Line = 0
                0x8D, 0x14, // Charge pump for internal Vcc generation
                0x20, 0x00, // Memory Addressing Mode = Page Addressing (default)
                0xA0, // No segment remap: Column 0 mapped to SEG0
                0xC8, // COM output scan direction = high to low
                0xDA, 0x12, // COM pin hardware config: No left/right remap; Alternative pin configuration
                0x81, 0xCF, // Display contrast: suitable for internal Vcc generation
                0xD9, 0xF1, // Precharge: suitable for internal Vcc generation
                0xDB, 0x40, // Vcomh Deselect Level: 0.77 Vcc (default)
                0xA4, // Generate display from CGRAM
                0xA6, // Positive image
                0xAF // Wakeup
                })
            };
        const uint IicRetryLimit = 3;

        public DisplayDriver(ThreadSafeI2CDevice iicDevice = null)
            {
            this.iicDevice = iicDevice ?? new ThreadSafeI2CDevice(new I2CDevice.Configuration(0x3C, 100), retryLimit: 3);
            }

        void IicTransact(I2CDevice.I2CTransaction[] transactions)
            {
            try
                {
                iicDevice.Execute(transactions);
                }
            catch (IOException) {}
            }

        void IicTransact(params ITransactionBuilder[] transactions)
            {
            try
                {
                iicDevice.Execute(transactions);
                }
            catch (IOException) {}
            }

        public void PowerUp()
            {
            var builder = DisplayTransactionBuilder.CommandModeTransaction()
                .SleepMode(true)
                .ConfigureClockDivider(0, 0)
                .MultiplexRatio(64)
                .DisplayOffset(0)
                .DisplayStartLine(0)
                .ChargePump(true)
                .HorizontalAddressingMode()
                .SegmentRemap(false)
                .CommonScanDirection(false)
                .CommonOutputConfiguration(remapped: false, interleaved: true)
                .DisplayContrast(0xCF)
                .PrechargePeriod(2, 2)
                .VcomhDeselectLevel(2)
                .TestMode(false)
                .DisplayPolarity(true)
                .SleepMode(false);
            IicTransact(builder);
            }

        public void TestMode()
            {
            IicTransact(DisplayTransactionBuilder.CommandModeTransaction().TestMode(true));
            }

        public void CancelTestMode()
            {
            IicTransact(DisplayTransactionBuilder.CommandModeTransaction().TestMode(false));
            }

        public void ClearScreen()
            {
            IicTransact(
                DisplayTransactionBuilder.CommandModeTransaction().HorizontalAddressingMode().SetWriteWindow(0, 7, 0, 127));
            for (var page = 0; page < 8; page++)
                {
                IicTransact(DisplayTransactionBuilder.DataModeTransaction().BlankPage());
                }
            }


        /// <summary>
        ///     Paints a series of stripes onto the display at a specified page and column, overwriting the previous contents.
        /// </summary>
        /// <param name="page">The page to start painting the first stripe.</param>
        /// <param name="column">The column position within the page to start painting the stripes.</param>
        /// <param name="stripes">The collection of stripes to be painted.</param>
        /// <summary>
        ///     Renders a character in a given font at the specified display location. The character must fit
        ///     completely within the display otherwise it is simply not rendered. This is a simplistic approach to clipping
        ///     but was an acceptable trade-off for performance. A later version may improve upon this simplistic approach.
        /// </summary>
        /// <param name="font">The font.</param>
        /// <param name="page">The page (row) address in display memory.</param>
        /// <param name="column">The column address in display memory.</param>
        /// <param name="character">
        ///     The character to be rendered. The font may substitute a different character if it doesn't have
        ///     a bitmap for the specified one; typically this will be the space character but it is font-dependent.
        /// </param>
        public byte RenderCharacterAt(BitMappedFont font, byte page, byte column, char character)
            {
            const int maxPage = 7;
            const int maxColumn = 127;
            var bitmap = font.GetBitmap(character);
            var endPage = page + font.CellPageHeight - 1;
            var endColumn = column + bitmap.Width - 1;
            if (endPage > maxPage) return 0;
            if (endColumn > maxColumn) return 0;
            IicTransact(DisplayTransactionBuilder.CommandModeTransaction().HorizontalAddressingMode().SetWriteWindow(page, endPage, column, endColumn));
            IicTransact(DisplayTransactionBuilder.DataModeTransaction().Append(bitmap.Stripes));
            return (byte) bitmap.Width;
            }

        public void RenderString(BitMappedFont font, byte startPage, byte startColumn, string text)
            {
            var cursor = startColumn;
            foreach (var c in text.ToCharArray())
                {
                cursor += RenderCharacterAt(font, startPage, cursor, c);
                cursor += font.Tracking;
                }
            }

        public
            int DisplayColumns
            {
            get { return 128; }
            }

        public
            int DisplayPages
            {
            get { return 8; }
            }
        }
    }