﻿// This file is part of the TA.NetMF.SSD1306 project
// 
// Copyright © 2015-2015 Tigra Astronomy, all rights reserved.
// 
// File: GlobalAssemblyInfo.cs  Last modified: 2015-09-11@05:55 by Tim Long

using System.Reflection;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyConfiguration("uncontrolled build")]
[assembly: AssemblyCompany("Tigra Astronomy")]
[assembly: AssemblyProduct("TA.NetMF.Devices.SSD1306")]
[assembly: AssemblyCopyright("Copyright © 2015 Tigra Astronomy, all rights reserved")]
[assembly: AssemblyVersion("0.2.0.*")]
[assembly: AssemblyFileVersion("0.2.0.0")]
[assembly: AssemblyInformationalVersion("0.2.0.0 Uncontrolled Build")]