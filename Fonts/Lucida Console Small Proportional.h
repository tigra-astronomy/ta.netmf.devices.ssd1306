// Font data for Lucida Console 12pt
extern const uint_8 lucidaConsole_12ptBitmaps[];
extern const FONT_INFO lucidaConsole_12ptFontInfo;
extern const FONT_CHAR_INFO lucidaConsole_12ptDescriptors[];

