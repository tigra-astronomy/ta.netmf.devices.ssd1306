// Font data for Lucida Console 32pt
extern const uint_8 lucidaConsole_32ptBitmaps[];
extern const FONT_INFO lucidaConsole_32ptFontInfo;
extern const FONT_CHAR_INFO lucidaConsole_32ptDescriptors[];

